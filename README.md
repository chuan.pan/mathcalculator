#MathCalculator Program

1. The program is written in JAVA.
1. Assume this program will have other mathematical sequences or series in the future, I use Strategy Pattern for implementation. 
   + eg: Other sequences or series could be "Figurate Numbers series like square, triangular, pentagonal, hexagonal no. series".  
1. In current design, its calculate algorithm and printer can be changed at runtime.
1. It only has one series and two type of printers are implemented. 
   + Fibonacci Series
   + Printer - Print All Numbers
   + Printer - Print The Last Number
 
##Unit Test Cases - TestSeriesCalculator.java
1. Test Fibonacci(1)
1. Test Fibonacci(5)
1. Test Fibonacci(-1)
1. Test Printer(print all numbers) for Fibonacci(5)
1. Test Printer(print the last number) for Fibonacci(10)


##Programs
```java
// main program
com.brighterion.Main.java
```
Execution Result:
```
Please enter a number to get Fibonacci series or [exit] to quit the program:
5
0 1 1 2 3
Please enter a number to get Fibonacci series or [exit] to quit the program:
exit
Thanks!

Process finished with exit code 0
```
