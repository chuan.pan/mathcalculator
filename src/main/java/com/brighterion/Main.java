package com.brighterion;

import com.brighterion.series.Fibonacci.FibonacciCalculator;
import com.brighterion.series.SeriesCalculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        SeriesCalculator calculator = new SeriesCalculator();
        calculator.setCalculateStrategy(new FibonacciCalculator());

        String number = "";
        while (number.equals("") || !number.equals("exit")) {
            System.out.println("Please enter a number to get Fibonacci series or [exit] to quit the program:");
            Scanner scanner = new Scanner(System.in);
            number = scanner.nextLine();

            if (!number.equals("exit")) {
                if (!number.matches("\\d*")) {
                    System.err.println("The number should be greater than or equal to zero. Please enter again:");
                } else {
                    calculator.doCalculate(Integer.parseInt(number));
                    calculator.print();
                    System.out.println();
                }
            }
        }

        System.out.println("Thanks!");
    }
}
