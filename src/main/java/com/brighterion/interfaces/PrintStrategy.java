package com.brighterion.interfaces;

import java.math.BigInteger;
import java.util.List;

public interface PrintStrategy {
    void print(List<BigInteger> inputs);
}
