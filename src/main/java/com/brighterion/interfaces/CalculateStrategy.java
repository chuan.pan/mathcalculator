package com.brighterion.interfaces;

import java.math.BigInteger;
import java.util.List;

public interface CalculateStrategy {
    List<BigInteger> calculate(int input);
}
