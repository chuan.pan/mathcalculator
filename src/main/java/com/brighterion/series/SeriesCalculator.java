package com.brighterion.series;

import com.brighterion.interfaces.CalculateStrategy;
import com.brighterion.interfaces.PrintStrategy;
import com.brighterion.series.Fibonacci.FibonacciCalculator;
import com.brighterion.series.Printer.PrintAllNumbers;

import java.math.BigInteger;
import java.util.List;

public class SeriesCalculator {
    // default calculator is Fibonacci
    private CalculateStrategy series = new FibonacciCalculator();
    // default printer is Print All Numbers
    private PrintStrategy printer = new PrintAllNumbers();
    private List<BigInteger> result;

    public void setCalculateStrategy(CalculateStrategy calculateStrategy) {
        this.series = calculateStrategy;
    }

    public void setPrinter(PrintStrategy printer) {
        this.printer = printer;
    }

    public void doCalculate(int input) {
        this.result = series.calculate(input);
    }

    public List<BigInteger> getResult() {
        return result;
    }

    public void print() {
        this.printer.print(this.result);
    }
}
