package com.brighterion.series.Fibonacci;

import com.brighterion.interfaces.CalculateStrategy;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class FibonacciCalculator implements CalculateStrategy {

    public List<BigInteger> calculate(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("The number should be greater or equal to zero");
        }
        List<BigInteger> result = new ArrayList<BigInteger>();

        // set it to the number of elements in Fibonacci Series
        BigInteger previousNumber = BigInteger.valueOf(0);
        BigInteger nextNumber = BigInteger.valueOf(1);

        for (int i = 0; i < number; ++i) {
            result.add(previousNumber);
            BigInteger sum = previousNumber.add(nextNumber);
            previousNumber = nextNumber;
            nextNumber = sum;
        }

        return result;
    }
}
