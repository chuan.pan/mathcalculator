package com.brighterion.series.Printer;

import com.brighterion.interfaces.PrintStrategy;

import java.math.BigInteger;
import java.util.List;

public class PrintTheLastNumber implements PrintStrategy {

    public void print(List<BigInteger> inputs) {
        int the_last_pos = inputs.size();
        System.out.print(inputs.get(the_last_pos - 1));
    }
}
