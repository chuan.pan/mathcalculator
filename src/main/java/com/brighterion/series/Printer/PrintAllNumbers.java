package com.brighterion.series.Printer;

import com.brighterion.interfaces.PrintStrategy;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

public class PrintAllNumbers implements PrintStrategy {

    public void print(List<BigInteger> inputs) {
        StringBuilder sb = new StringBuilder();
        Iterator<BigInteger> iterator = inputs.iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next());
            // append space if not the last element
            if (iterator.hasNext()) {
                sb.append(" ");
            }
        }
        System.out.print(sb.toString());
    }
}
