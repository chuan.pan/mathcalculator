import com.brighterion.series.Fibonacci.FibonacciCalculator;
import com.brighterion.series.Printer.PrintAllNumbers;
import com.brighterion.series.Printer.PrintTheLastNumber;
import com.brighterion.series.SeriesCalculator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestSeriesCalculator {
    private SeriesCalculator seriesCalculator = new SeriesCalculator();
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    // Test Fibonacci(1)
    @Test
    public void testFib1() {
        List<BigInteger> correctResult = new ArrayList<BigInteger>();
        correctResult.add(BigInteger.valueOf(0));

        seriesCalculator.setCalculateStrategy(new FibonacciCalculator());
        seriesCalculator.doCalculate(1);
        List<BigInteger> result = seriesCalculator.getResult();
        assertEquals(correctResult, result);
    }

    // Test Fibonacci(5)
    @Test
    public void testFib5() {
        List<BigInteger> correctResult = new ArrayList<BigInteger>();
        correctResult.add(BigInteger.valueOf(0));
        correctResult.add(BigInteger.valueOf(1));
        correctResult.add(BigInteger.valueOf(1));
        correctResult.add(BigInteger.valueOf(2));
        correctResult.add(BigInteger.valueOf(3));

        seriesCalculator.setCalculateStrategy(new FibonacciCalculator());
        seriesCalculator.doCalculate(5);
        List<BigInteger> result = seriesCalculator.getResult();
        assertEquals(correctResult, result);
    }

    // Test Fibonacci(-1)
    @Test(expected = IllegalArgumentException.class)
    public void testMinusOne() {
        seriesCalculator.setCalculateStrategy(new FibonacciCalculator());
        seriesCalculator.doCalculate(-1);
    }

    // Test Printer(print all numbers) for Fibonacci(5)
    @Test
    public void testPrintAllNumbers() {
        seriesCalculator.setCalculateStrategy(new FibonacciCalculator());
        seriesCalculator.setPrinter(new PrintAllNumbers());
        seriesCalculator.doCalculate(5);
        seriesCalculator.print();
        assertEquals("0 1 1 2 3", outContent.toString());
    }

    // Test Printer(print the last number) for Fibonacci(10)
    @Test
    public void testPrintTheLastNumber() {
        seriesCalculator.setCalculateStrategy(new FibonacciCalculator());
        seriesCalculator.setPrinter(new PrintTheLastNumber());
        seriesCalculator.doCalculate(10);
        seriesCalculator.print();
        assertEquals("34", outContent.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
}
